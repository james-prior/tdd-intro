# tdd-demo

Test Driven Development (TDD) Demo

This repo has the files for demonstrating
[Test Driven Development](https://en.wikipedia.org/wiki/Test-driven_development).
Test Driven Development is a process. It is often abbreviated TDD.
This repo has the files, but not the explanation of how and why
TDD works. That explanation, that narrative, is saved for live
presentations.

## prerequisites

You will need to know

- how to create and use virtual machines with `pipenv`
- how to use git
- how to use an editor
- how to use bash

You will need to have

- a computer running a Unix-like operating system such as macos or Linux
- access to this git repository

## set up

Clone this git repository to your computer running a Unix-like operating system
such as macos or Linux.

Execute in the same directory as this `README.md` file is in:

`pipenv --python=3 install pytest-xdist`

## edit files and run tests

Arrange your windows and/or panes so that all the time you can simultaneously
see the tests and the both files `tests/test_fizzbuzz.py` and `fizzbuzz.py`.

In the same directory as this `README.md` file is in,
open the files `tests/test_fizzbuzz.py` and `fizzbuzz.py`
in the editor of your choice.

Execute in the same directory as this `README.md` file is in:

`pipenv run pytest --color=yes -f .`

## the process

The TDD process is roughly as follows.

1. Add a test.
2. (RED): Run all tests and see if the new test fails. (The new test _must_ fail).
3. (GREEN): Write the code to pass the tests, then run all tests.  The tests
   should all pass, now. If they do not all pass, then debug and fix the code,
   the tests, or both, until all the tests pass.
4. (REFACTOR): Refactor code.
5. Repeat steps 3 through 4 or 1 through 4.

This demo uses the example of writing a function in Python that returns
what a fizzbuzz player should say for a given number. It avoids importing any
libraries except pytest and pytest-xdist. This repo has files at various stages
of the TDD process.

One of the subtleties of TDD is how big or small each change is.
When things are going well, larger changes are OK. When progress
is tough, make small changes.

## miscellaneous scraps

    What's the importance of writing a failing test?

    What's the simplest thing that could possibly work?

        What's the importance of writing just enough code to make the test pass?

    commit whatever sin you have to commit to get the test to pass (THEN refactor)

    tdd has a profound effect on the structure of software

    prepare tdd intro repo

        prepare with own resources, put on personal github account

        simpler

            use the editor of your choice
            want to see three things simultaneously:

                test code
                code
                tests

        practices

            red: write a failing test
            green: write just barely enough code to make the test pass
            refactor:
